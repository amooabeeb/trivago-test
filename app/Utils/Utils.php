<?php


namespace App\Utils;


class Utils
{

    public static function deduceReputationBadge($reputation)
    {
        if ($reputation <= 500) {
            return 'red';
        } else if ($reputation >= 500 && $reputation <= 799)  {
            return 'yellow';
        }
        return 'green';
    }
}
