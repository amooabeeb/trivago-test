<?php

namespace App\Utils;

class Error
{
    const INVALID_USER = 'User does not exist';
    const AUTH = 'Invalid email or password';

    const ITEM_NOT_FOUND = 'Item not found';
    const ITEM_NOT_EXISTING = 'Item not existing';
    const ITEM_NOT_AVAILABLE = 'Item not available for booking';

    const FATAL = 'Something went wrong';
}
