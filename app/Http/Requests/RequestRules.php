<?php

namespace App\Http\Requests;


use Illuminate\Validation\Rule;

class RequestRules
{

    public static function authenticate()
    {
        return [
            'email' => 'required|email|max:255',
            'password' => 'required|min:3'
        ];
    }

    public static function createItem()
    {
        $allowed_categories = ['hotel', 'alternative', 'hostel', 'lodge', 'resort', 'guest-house'];
        return [
            'name' => 'required|min:10|max:255',
            'rating' => 'required|numeric|gte:0|lte:5',
            'category' => ['required', Rule::in($allowed_categories)],
            'price' => 'required|numeric|gt:0',
            'availability' => 'required|numeric|gt:0',
            'reputation' => 'required|numeric|gte:0|lte:1000',
            'image' => 'required|url|ends_with:jpg,png,svg',
            'location.zip' => 'required|numeric|digits:5|gt:0',
            'location.state' => 'required|min:3',
            'location.city' => 'required|min:3',
            'location.country' => 'required|min:3',
            'location.address' => 'required|min:5'
        ];
    }
}
