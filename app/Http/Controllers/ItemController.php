<?php


namespace App\Http\Controllers;


use App\Exceptions\BadRequestException;
use App\Repositories\Contracts\ItemRepositoryInterface;
use App\Utils\Error;
use Exception;
use Illuminate\Http\Request;

class ItemController extends Controller
{

    public $itemRepository;

    public function __construct(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function getItems(Request $request)
    {
        try {
            $listings = $this->itemRepository->getItemsByUserId($request->auth->id, $request->query());
            return $this->sendSuccessResponse($listings);
        } catch (Exception $e) {
            dd($e->getMessage());
            return $this->sendFatalErrorResponse();
        }
    }

    public function getSingleItem(Request $request, $id)
    {
        try {
            $item = $this->itemRepository->getItemByUserId($id, $request->auth->id);
            if (!$item) {
                return $this->sendErrorResponse(Error::ITEM_NOT_FOUND, null, 404);
            }
            return $this->sendSuccessResponse($item);
        } catch (Exception $e) {
            return $this->sendFatalErrorResponse();
        }
    }

    public function createItem(Request$request)
    {
        try {
            $data = $this->validateCreateItem($request->all());
            $data['location']['zip_code'] = $data['location']['zip'];
            $item = $this->itemRepository->createItem($data, $request->auth->id);
            return $this->sendSuccessResponse($item);
        } catch (BadRequestException $e) {
            return $this->sendErrorResponse($e->getMessage(), null,  $e->getStatusCode());
        } catch (Exception $e) {
            return $this->sendFatalErrorResponse();
        }
    }

    public function deleteItem(Request $request, $id)
    {
        try {
            $item = $this->itemRepository->getItemByUserId($id, $request->auth->id);
            if (!$item) {
                return $this->sendErrorResponse(Error::ITEM_NOT_FOUND, null, 404);
            }
            $this->itemRepository->deleteItem($id);
            return $this->sendSuccessResponse([]);
        } catch (Exception $e) {
            return $this->sendFatalErrorResponse();
        }
    }

    public function bookItem(Request $request, $id)
    {
        try {
            $item = $this->itemRepository->getItemById($id);
            if (!$item) {
                return $this->sendErrorResponse(Error::ITEM_NOT_EXISTING, null, 400);
            } else if ($item->availability < 1) {
                return $this->sendErrorResponse(Error::ITEM_NOT_AVAILABLE, null, 400);
            }
            $availability = $item->availability - 1;
            $item = $this->itemRepository->updateItem($item->id, ['availability' => $availability]);
            return $this->sendSuccessResponse($item);
        }  catch (Exception $e) {
            return $this->sendFatalErrorResponse();
        }
    }

}
