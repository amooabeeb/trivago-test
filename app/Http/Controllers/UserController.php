<?php


namespace App\Http\Controllers;


use App\Exceptions\BadRequestException;
use App\Repositories\Contracts\ItemRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Utils\Error;
use App\Utils\JWTIssuer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public $userRepository;
    public $itemRepository;

    public function __construct(UserRepositoryInterface $userRepository, ItemRepositoryInterface $itemRepository)
    {
        $this->userRepository = $userRepository;
        $this->itemRepository = $itemRepository;
    }

    public function authenticate(Request $request)
    {
        try {
            $data = $this->validateAuth($request->all());
            $user = $this->userRepository->getUserByEmail($data['email']);
            if (!$user) {
                return $this->sendErrorResponse(Error::INVALID_USER,  null, 401);
            } else if (!Hash::check($data['password'], $user->password)) {
                return $this->sendErrorResponse(Error::AUTH,  null, 401);
            }
            $tokenizer = new JWTIssuer($user);
            $response = [
                'token_data' => $tokenizer->getToken(),
                'meta' => $user
            ];
            return $this->sendSuccessResponse($response);
        } catch (BadRequestException $e) {
            return $this->sendErrorResponse($e->getMessage(), null,  $e->getStatusCode());
        } catch (Exception $e) {
            return $this->sendFatalErrorResponse();
        }
    }
}
