<?php

namespace App\Http\Controllers;

use App\Traits\JSONResponse;
use App\Traits\RequestValidator;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use JSONResponse, RequestValidator;
}
