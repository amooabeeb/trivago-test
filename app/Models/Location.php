<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;

class Location extends BaseModel
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'zip_code', 'state', 'country', 'address', 'city', 'item_id'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
