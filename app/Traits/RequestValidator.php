<?php
/**
 * Created by PhpStorm.
 * User: abeeb
 * Date: 6/1/18
 * Time: 4:22 PM
 */

namespace App\Traits;

use App\Exceptions\BadRequestException;
use App\Http\Requests\RequestRules;
use Illuminate\Support\Facades\Validator;

trait RequestValidator
{

    public function validateAuth($data)
    {
        $validator = Validator::make($data, RequestRules::authenticate());
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            throw new BadRequestException($error, 400);
        }
        return $validator->validated();
    }

    public function validateCreateItem($data)
    {
        $validator = Validator::make($data, RequestRules::createItem());
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            throw new BadRequestException($error, 400);
        }
        return $validator->validated();
    }

}
