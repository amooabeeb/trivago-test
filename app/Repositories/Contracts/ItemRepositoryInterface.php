<?php


namespace App\Repositories\Contracts;


interface ItemRepositoryInterface
{

    public function getItemsByUserId($user_id, $filters);
    public function getItemByUserId($id, $user_id);
    public function getItemById($id);
    public function deleteItem($id);
    public function updateItem($id, $data);
    public function bookItem($id, $data);
    public function createItem($data, $user_id);
}
