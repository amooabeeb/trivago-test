<?php


namespace App\Repositories\Concrete;


use App\Models\Item;
use App\Models\Location;
use App\Repositories\Contracts\ItemRepositoryInterface;
use App\Utils\Utils;

class ItemRepository implements ItemRepositoryInterface
{

    public $item;

    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    public function getItemsByUserId($user_id, $filters, $perPage = 10)
    {
        $items = $this->item->where('user_id', $user_id);
        if (isset($filters['category'])) {
            $items->where('category', $filters['category']);
        }
        if (isset($filters['rating'])) {
            $items->where('rating', $filters['rating']);
        }
        if (isset($filters['reputation_badge'])) {
            $items->where('reputation_badge', $filters['reputation_badge']);
        }
        if (isset($filters['availabilty_more_than'])) {
            $items->where('availability_more_than', '>', $filters['availability_more_than']);
        }
        if (isset($filters['availabilty_less_than'])) {
            $items->where('availabilty_less_than', '<', $filters['availabilty_less_than']);
        }
        if (isset($filters['city'])) {
            $items->whereHas('location', function($query) use ($filters) {
                $query->where('city', $filters['city']);
            });
        }
        return $items->with('location')->paginate($perPage);
    }

    public function getItemByUserId($id, $user_id)
    {
        return $this->item->where(['id' => $id, 'user_id' => $user_id])->with('location')->first();
    }

    public function deleteItem($id)
    {
        return $this->item->where('id', $id)->delete();
    }


    public function updateItem($id, $data)
    {
        $this->item->where('id', $id)->update($data);
        return $this->item->where('id', $id)->first();
    }

    public function bookItem($id, $data)
    {
        // TODO: Implement bookItem() method.
    }

    public function createItem($data, $user_id)
    {
       $item = new Item();
       $item->name = $data['name'];
       $item->availability = $data['availability'];
       $item->reputation = $data['reputation'];
       $item->rating = $data['rating'];
       $item->category = $data['category'];
       $item->image = $data['image'];
       $item->price = $data['price'];
       $item->user_id = $user_id;
       $item->reputation_badge = Utils::deduceReputationBadge($data['reputation']);
       $item->save();
       $this->attachItemToLocation($data['location'], $item->id);
       return $item;

    }

    private function attachItemToLocation($data, $item_id)
    {
        $location = new Location();
        $location->zip_code = $data['zip_code'];
        $location->city = $data['city'];
        $location->state = $data['state'];
        $location->country = $data['country'];
        $location->address = $data['address'];
        $location->item_id = $item_id;
        $location->save();
        return $location;
    }

    public function getItemById($id)
    {
        return $this->item->where('id', $id)->first();
    }
}
