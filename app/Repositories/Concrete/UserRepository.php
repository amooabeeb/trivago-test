<?php


namespace App\Repositories\Concrete;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUserByEmail(string $email)
    {
        return $this->user->where('email', $email)->first();
    }
}
