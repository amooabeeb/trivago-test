<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/auth', 'UserController@authenticate');
$router->group(['middleware' => 'jwt-auth'], function () use ($router) {
    $router->get('/items', 'ItemController@getItems');
    $router->get('/items/{id}', 'ItemController@getSingleItem');

    $router->post('/items', 'ItemController@createItem');
    $router->post('/items/{id}/book', 'ItemController@bookItem');

    $router->delete('/items/{id}', 'ItemController@deleteItem');


});
