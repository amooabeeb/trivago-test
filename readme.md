# Basic Hotel API
The API for trivago backend test

# Get started
- Ensure docker is installed
- `cd` into the project directory
- Run `docker-compose up -d` to startup the application
- Run ` docker-compose exec app php artisan migrate` to migrate database
- Run `docker-compose exec app php artisan db:seed` to seed test hotelier
- The application can now be accessed on `localhost:8050`

# Testing the API
The API documentation can be found at 
https://documenter.getpostman.com/view/7210435/SW7UbAie

Every endpoint apart from `/auth` requires an authentication token. 
The token can be gotten by providing the following test credentials to `/auth`

```$xslt
{
    "email": "test@email.com",
    "password": "password123"
}
```

# Versioning
V1

