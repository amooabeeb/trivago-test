<?php


use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->first_name = 'First';
        $user->last_name = 'Last';
        $user->email = 'test@email.com';
        $user->password = Hash::make('password123');
        $user->save();

    }
}
